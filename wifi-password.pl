use warnings;
use strict;

sub extract_value {
    my ($data, $key) = @_;
    my $value = undef;
    for (split "\n", $data) {
	if (/$key\s+:\s+(.*)/) {
	    $value = $1;
	    last;
	}
    }
    $value;
}

my $interface = eval { `netsha wlan show interface` };
die $! unless $interface;
my $SSID = extract_value $interface, "SSID";
unless ($SSID) {
    print "Not connected to Wi-Fi\n";
    exit -1;
}
my $network = eval { `chcp 437 && netsh wlan show profiles name=$SSID key=clear` } or die $!;
my $auth_type = extract_value $network, "Authentication";
my $password = extract_value $network, "Key Content";

print <<OUTPUT;
SSID       : $SSID
Password   : $password
Auth type  : $auth_type
OUTPUT

exit 0;

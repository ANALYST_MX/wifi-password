# WiFi-Password

## How to use
To get the password for the WiFi you're currently logged onto:

```cmd
perl wifi-password.pl
```